import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from './user.model';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {environment} from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  public currentUser$ = new BehaviorSubject<User>(undefined);
  private readonly CURRENT_USER = 'currentuser';

  register(payload): Observable<User> {
    return this.http.post<User>('api/register', payload);
  }

  login(payload): Observable<User> {
    console.log(`login at api/login`);
    return this.http.post<User>('api/login', payload);
  }

  logout(): void {
    localStorage.removeItem(this.CURRENT_USER);
  }

  getUserFromLocalStorage(): Observable<User> {
    return of(JSON.parse(localStorage.getItem(this.CURRENT_USER)));
  }

  userExists(): boolean {
    return localStorage.getItem(this.CURRENT_USER) != null;
  }

  saveUserToLocalStorage(user: User): void {
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
    this.currentUser$.next(user);
  }

}

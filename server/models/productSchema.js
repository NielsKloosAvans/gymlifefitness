const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  categories:{
    type: String
  },
  price: {
    type: Number,
    required: [true, 'Price is required']
  },

})

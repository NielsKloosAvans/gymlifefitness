describe('Login', () => {
    it('Visits my app', () => {
      cy.visit('/login')
  
      cy.url().should('include', '/login')
      cy.get('mat-label').contains('Email').should('have.text', 'Email')
      cy.get('mat-label').contains('Password').should('have.text', 'Password')
    })
  })
  
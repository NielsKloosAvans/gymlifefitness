const User = require('../models/user')

module.exports = {
  login(req, res, next) {
    const login = {
      email: req.body.email,
      password: req.body.password
    }
    User.findOne({
      email: req.body.email
    }).then((user) => {
      if (user != null) {
        user.compareUserPassword(login.password, user.password)
          .then((exists) => {
            if (exists === true) {
              const token = user.generateJwtToken({user}, "secret", {
                expiresIn: 604800
              })
              res.cookie('Token', token)
              res.status(200).json({
                user,
                token
              })
            } else {
              res.status(401).send({
                error: 'Email or password incorrect'
              })
            }
          }).catch(next)
      } else {
        res.status(401).send({
          error: 'User not found'
        })
      }
    }).catch(next)
  },
  register(req, res, next) {
    User.findOne({
      email: req.body.email
    }).then((user) => {
      if (user != null) {
        res.status(409).send({
          error: 'User already exists with this email.'
        })
      } else {
        const newUser = new User({
          email: req.body.email,
          name: req.body.name,
        })
        newUser.password = newUser.hashPassword(req.body.password)
        newUser.save()
          .then((user) => {
            res.status(200).json({
              msg: "New user created",
              data: user
            })
          })
      }
    })
      .catch(next)
  }
}

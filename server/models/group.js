const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ShoppingListSchema = require('../models/shopList').schema

const GroupSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: [true, 'Group needs an owner']
  },
  members: [{
    type: Schema.Types.ObjectId,
    ref: 'user'
  }],
  shopLists: [ShoppingListSchema]
})

GroupSchema.pre('remove', (next) => {
  const UsersInGroup = mongoose.model('user');
  UsersInGroup.updateMany({groups: {$in: this._id}}, {groups: {$pull: this._id}})
    .then(() => next())
})


module.exports = mongoose.model('group', GroupSchema)

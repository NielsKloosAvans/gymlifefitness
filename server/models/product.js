const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const ProductSchema = require('./productSchema')

module.exports =  mongoose.model('product',ProductSchema)

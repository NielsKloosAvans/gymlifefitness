import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {ShoppingList} from './ShoppingList.model';
import {MatSnackBar} from '@angular/material/snack-bar';
import {catchError, tap} from 'rxjs/operators';
import {ErrorHandlerService} from '../../helpers/error-handler.service';
import {CookiesHandlerService} from '../../helpers/cookies-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ShoppinglistService {

  constructor(
    private http: HttpClient,
    private cookiesHandler: CookiesHandlerService,
    private snackBar: MatSnackBar,
    private errorHandler: ErrorHandlerService) {
  }

  getList(): Observable<ShoppingList[]> {
    console.log(this.cookiesHandler.get('Token'));
    if (this.cookiesHandler.get('Token') != null) {
      return this.http.get<ShoppingList[]>('api/shop-list', this.cookiesHandler.httpOptions).pipe(
        tap((res: ShoppingList[]) => {
          console.log('res: ', res);
        }),
        catchError(error => of(this.errorHandler.handleError('getList', error)))
      );
    }
    return null;
  }
  getDetails(id): Observable<ShoppingList> {
    return this.http.get<ShoppingList>(`api/shop-list/${id}`, this.cookiesHandler.httpOptions);
  }

  getById(id: string): Observable<ShoppingList> {
    return this.http.get<ShoppingList>(`api/shop-list/${id}`, this.cookiesHandler.httpOptions);
  }

  removeItems(payload, id): Observable<ShoppingList> {
    return this.http.put<ShoppingList>(`api/shop-list/${id}/remove`, payload, this.cookiesHandler.httpOptions);
  }

  createList(payload): Observable<ShoppingList> {
    return this.http.post<ShoppingList>('api/shop-list', payload, this.cookiesHandler.httpOptions)
      .pipe(
        tap(() => {
          this.snackBar.open('List created!', '', {
            duration: 2000
          });
        }),
        catchError(error => of(this.errorHandler.handleError('createList', error)))
      );
  }
  updateShoppingList(id, payload): Observable<ShoppingList> {
    return this.http.put<ShoppingList>(`/api/shop-list/${id}`, payload, this.cookiesHandler.httpOptions);
  }
}

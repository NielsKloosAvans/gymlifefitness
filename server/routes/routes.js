const ProductController = require("../controllers/product_controller")
const UserController = require("../controllers/user_controller")
const ShopListController = require("../controllers/shop_list_controller")
const GroupController = require("../controllers/group_controller")
const auth = require('../middleware/auth')

module.exports = (app) => {
  //Auth
  app.post('/api/register', UserController.register)
  app.post('/api/login', UserController.login)

  //Groups
  app.get('/api/groups/', auth, GroupController.index)
  app.get('/api/groups/:id', GroupController.details)
  app.post('/api/groups', auth, GroupController.create)
  app.put('/api/groups/:id', auth, GroupController.put)
  app.delete('/api/groups/:id', auth, GroupController.delete)

  //Products
  app.get('/api/products', ProductController.index)
  app.get('/api/products/:id', ProductController.details)
  app.post('/api/products', ProductController.create)
  app.put('/api/products/:id', ProductController.put)
  app.delete('/api/products/:id', ProductController.delete)

  //shopList
  app.get('/api/shop-list', auth, ShopListController.index)
  app.get('/api/shop-list/:id', auth, ShopListController.details)
  app.post('/api/shop-list', auth, ShopListController.create)
  app.put('/api/shop-list/:id', auth, ShopListController.put)
  app.put('/api/shop-list/:id/add', auth, ShopListController.addItem)
  app.put('/api/shop-list/:id/remove', auth, ShopListController.removeItem)
  app.delete('/api/shop-list/:id', auth, ShopListController.delete)
}

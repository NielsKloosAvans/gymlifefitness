import {Component, OnInit} from '@angular/core';
import {ShoppinglistService} from '../shoppinglist.service';
import {ShoppingList} from '../ShoppingList.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  lists: ShoppingList[];
  id: string
  selected: string[];
  
  constructor(private listService: ShoppinglistService,private route: ActivatedRoute) {
  }

  getLists(): void {
    this.listService.getList().subscribe(res => {
      this.lists = res;
    });
  }
  onAreaListControlChanged(list): void {
    this.selected = list.selectedOptions.selected.map(item => item.value);
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getLists();
  }
}

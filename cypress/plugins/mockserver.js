const { on } = require("events");
const express = require("express");
const port = 4200;

let app = express();
let routes = require("express").Router();

// Global mock objects
const mockUserData = {
  name: { firstName: "Firstname", lastName: "Lastname" },
  emailAdress: "first.last@avans.nl",
  token:
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzUwMjIzNjQsImlhdCI6MTU3NDE1ODM2NCwic3ViIjp7ImVtYWlsIjoiYWRtaW5AYXZhbnMubmwiLCJpZCI6IjVkYzlhY2Y3NmUzOTVhMTY1ODkwMjk2MiJ9fQ.qRPy-lTPIopAJPrarJYZkxK0suUJF_XZ9szeTtie4nc",
};

const mockProduct = [
  {
    _id: "61950e163526e4c9533dd4f5",
    categories: "fruit",
    name:"appel",
    price:33,
    __v: 0,
  },
  {
    _id: "61950e163546e4c9533dd4f5",
    categories: "snoep",
    name:"zure mat",
    price:3,
    __v: 0,
  },
  {
    _id: "61950e193526e4c9533dd4f5",
    categories: "drinken",
    name:"appelsap",
    price:2,
    __v: 0,
  },
];

const mockShoplist = [
  {
    _id: "61950e163525e4c9533dd4f5",
    name:"boodschappen voor dit weekend",
    products: {
      categories: "drinken",
      name:"appelsap",
      price:2,
      __v: 0,
    }
  },
  {
    _id: "61950e163535e4c9533dd4f5",
    name:"boodschappen voor door de weeks",
    products: {
      categories: "drinken",
      name:"koffie",
      price:2,
      __v: 0,
    }
  },
];


app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

routes.post("api/auth/login", (req, res, next) => {
  res.status(200).json(mockUserData);
});

routes.post("api/auth/register", (req, res, next) => {
    res.status(200).json(mockUserData);
  });
  

routes.get("api/products", (req, res, next) => {
  res.status(200).json(mockProduct);
});

routes.get("api/list", (req, res, next) => {
  res.status(200).json(mockShoplist);
});
app.use(routes);


app.use("*", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
});

process.on("uncaughtException", (err) => {
  console.log("There was an uncaught error", err);
  process.exit(1); 
});

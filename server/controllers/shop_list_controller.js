const ShopList = require('../models/shopList')
const Group = require('../models/group')

module.exports = {
  index(req, res, next) {
    Group.find({members: {$in: req.userData._id}})
      .then((groups) => {
        let lists = []
        lists.map((groups) => groups.shopLists)
        groups.forEach((g) => {
          lists = lists.concat(g.shopLists)
        })
        return Promise.all(lists);
      })
      .then((result) => res.send(result))
      .catch(next)
  },
  details(req, res, next) {
    ShopList.findById({_id: req.params.id})
      .then(data => res.send(data))
      .catch(next)
  },
  create(req, res, next) {
    ShopList.create({
      name: req.body.name
    })
      .then(l => {
        return Group.findOneAndUpdate(
          {_id: req.body.group},
          {$push: {shopLists: [l]}})
          .then(user => res.send(user))
      })
      .catch(next)
  },
  put(req, res, next) {
    ShopList.findByIdAndUpdate({_id: req.params.id}, req.body)
      .then(() => ShopList.findById({_id: req.params.id}))
      .then(data => res.send(data))
      .catch(next)
  },
  addItem(req, res, next) {
    ShopList.findOneAndUpdate({_id: req.params.id}, {$push: {products: [req.body]}})
      .then(() => ShopList.findById({_id: req.params.id}))
      .then(data => res.send(data))
      .catch(next)
  },
  removeItem(req, res, next) {
    ShopList.findOneAndUpdate({_id: req.params.id}, {$pull: {products: {_id: req.body}}})
      .then(() => ShopList.findById({_id: req.params.id}))
      .then(data => res.send(data))
      .catch(next)
  },
  delete(req, res, next) {
    ShopList.findByIdAndDelete({_id: req.params.id}, req.body)
      .then(() => ShopList.findById({_id: req.params.id}))
      .then(data => res.status(204).send({
        message: 'ShopList deleted.',
        body: data
      }))
      .catch(next)
  }
}

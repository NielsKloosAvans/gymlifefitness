export class User {
  id: string;
  name: string;
  email: string;
  password: string;
  friendRequests: User[];
  friends: User[];
  // inGroup: FamilyGroup;
}

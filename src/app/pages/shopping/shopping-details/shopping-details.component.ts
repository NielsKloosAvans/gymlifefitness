import {Component, Input, OnInit, Output} from '@angular/core';
import {Product} from '../../product/product.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ShoppingList} from '../ShoppingList.model';
import {ShoppinglistService} from '../shoppinglist.service';
import {GroupService} from '../../groups/group.service';
import {Group} from '../../groups/group.model';

@Component({
  selector: 'app-shopping-details',
  templateUrl: './shopping-details.component.html',
  styleUrls: ['./shopping-details.component.css']
})
export class ShoppingDetailsComponent implements OnInit {
  @Input() list: ShoppingList;
  @Output() details: ShoppingList;
  selected: string[];
  groups: Group[];
  products: Product[];
  title: string;
  id: string;
  selGroup: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private groupService: GroupService,
    private shoppingService: ShoppinglistService) {
  }

  onAreaListControlChanged(list): void {
    this.selected = list.selectedOptions.selected.map(item => item.value);
  }
  getList(): void {
    this.shoppingService.getById(this.id).subscribe(res => {
      this.title = res.name;
      this.products = res.products;
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getList();
  }

  edit(): void {
    this.router.navigate([`/list/${this.id}/edit`]);
  }

  deleteItems(): void {
    if (this.selected.length > 0) {
      console.log(`removing ${this.selected}`);
      this.shoppingService.removeItems(this.selected, this.id)
        .subscribe((res) => {
          console.log(res);
          this.products = res.products;
        });
    } else {
      console.log('None selected');
    }
  }
}

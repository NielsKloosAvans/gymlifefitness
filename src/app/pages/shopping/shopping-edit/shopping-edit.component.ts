import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ShoppingList} from '../ShoppingList.model';
import {ShoppinglistService} from '../shoppinglist.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandlerService} from '../../../helpers/error-handler.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @Input() shopping: ShoppingList;
  shoppingForm: FormGroup;
  subscription: Subscription;
  id: string;

  constructor(private formBuilder: FormBuilder,
              private shoppingService: ShoppinglistService,
              private route: ActivatedRoute,
              private router: Router,
              private errorHandler: ErrorHandlerService,
              private snackBar: MatSnackBar) {
    this.shoppingForm = formBuilder.group({
      name: new FormControl('', [Validators.required])
    });
  }

  onSubmit(): void {
    this.shoppingService.updateShoppingList(this.id, this.shoppingForm.value).subscribe(() => {
      this.snackBar.open('shopping list updated!', '', {
        duration: 2000,
      });
      this.router.navigate(['/list/', this.id]);
    }, (err: HttpErrorResponse) => {
      console.log(err);
      this.errorHandler.handleError('getById', err);
    });
  }

  getDetails(): void {
    this.subscription = this.shoppingService.getDetails(this.id).subscribe((res) => {
      if (res != null) {
        this.shopping = res;
      }
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getDetails();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const routes = require('./routes/routes');
require('dotenv').config()
const app = express();
app.use(cors());

mongoose.connect("mongodb+srv://niels:YQWcN6Ot0lH1D3Oi@thecirclecluster.rye07.mongodb.net/TheCircleCluster?retryWrites=true&w=majority",
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
  })
  .then(() => {
    console.log('Connected to database!');
  })
  .catch((er) => {
    console.log('Connection failed!' + er);
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

routes(app)

app.use(express.static(path.join(__dirname, '..', 'dist', 'angular')))

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
})

app.use((req, res) => {
  res.sendFile(path.join(__dirname, '..', 'dist', 'angular','index.html'));
});

module.exports = app;

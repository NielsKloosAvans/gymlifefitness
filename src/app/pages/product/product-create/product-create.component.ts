import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ProductService} from '../product.service';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  productForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private productService: ProductService) {
    this.productForm = formBuilder.group({
      name: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      categories: new FormControl(''),
    });
  }

  onSubmit(): void {
    console.log('button clicked');
    this.productService.createProduct(this.productForm.value).subscribe((data: any) => {
      console.log(data);
      this.router.navigateByUrl('/products').then((r) => console.log(r));
    });
  }

  ngOnInit(): void {
  }

}

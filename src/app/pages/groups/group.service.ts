import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ErrorHandlerService} from '../../helpers/error-handler.service';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Group} from './group.model';
import {CookiesHandlerService} from '../../helpers/cookies-handler.service';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private errorHandler: ErrorHandlerService,
    private cookiesHandler: CookiesHandlerService) {
  }

  createGroup(payload): Observable<Group> {
    return this.http.post<Group>('api/groups', payload, this.cookiesHandler.httpOptions)
      .pipe(
        tap(() => {
          this.snackBar.open('Group created!', '', {
            duration: 2000
          });
        }),
        catchError(error => of(this.errorHandler.handleError('createGroup', error)))
      );
  }

  getGroups(): Observable<Group[]> {
    console.log(this.cookiesHandler.get('Token'));
    if (this.cookiesHandler.get('Token') != null) {
      return this.http.get<Group[]>('api/groups', this.cookiesHandler.httpOptions);
    }
    return null;
  }

  getDetails(id): Observable<Group> {
    return this.http.get<Group>(`api/groups/${id}`, this.cookiesHandler.httpOptions);
  }

  updateGroup(id, payload): Observable<Group> {
    return this.http.put<Group>(`api/groups/${id}`, payload, this.cookiesHandler.httpOptions);
  }
  deleteGroup(id): Observable<Group> {
    return this.http.delete<Group>(`api/groups/${id}`, this.cookiesHandler.httpOptions);
  }
}

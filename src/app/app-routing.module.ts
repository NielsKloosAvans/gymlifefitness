import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {LayoutComponent} from './layout/layout.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {UsecasesComponent} from './pages/about/usecases/usecases.component';
import {ProductListComponent} from './pages/product/product-list/product-list.component';
import {ShoppingListComponent} from './pages/shopping/shopping-list/shopping-list.component';
import {ProductEditComponent} from './pages/product/product-edit/product-edit.component';
import {ShoppingEditComponent} from './pages/shopping/shopping-edit/shopping-edit.component';
import {ProductCreateComponent} from './pages/product/product-create/product-create.component';
import {ShoppingDetailsComponent} from './pages/shopping/shopping-details/shopping-details.component';
import {ShoppingCreateComponent} from './pages/shopping/shopping-create/shopping-create.component';
import {GroupListComponent} from './pages/groups/group-list/group-list.component';
import {GroupDetailsComponent} from './pages/groups/group-details/group-details.component';
import {GroupCreateComponent} from './pages/groups/group-create/group-create.component';
import {GroupEditComponent} from './pages/groups/group-edit/group-edit.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {path: 'dashboard', pathMatch: 'full', component: DashboardComponent},
      {path: '', pathMatch: 'full', redirectTo: 'dashboard'},
      {path: 'about', pathMatch: 'full', component: UsecasesComponent},

      // Products
      {path: 'products', pathMatch: 'full', component: ProductListComponent},
      {path: 'products/:id/edit', pathMatch: 'full', component: ProductEditComponent},
      {path: 'products/create', pathMatch: 'full', component: ProductCreateComponent},

      // Groups
      {path: 'groups', pathMatch: 'full', component: GroupListComponent},
      {path: 'groups/create', pathMatch: 'full', component: GroupCreateComponent},
      {path: 'groups/:id', pathMatch: 'full', component: GroupDetailsComponent},
      {path: 'groups/:id/edit', pathMatch: 'full', component: GroupEditComponent},

      // ShopLists
      {path: 'list', pathMatch: 'full', component: ShoppingListComponent},
      {path: 'list/create', pathMatch: 'full', component: ShoppingCreateComponent},
      {path: 'list/:id', pathMatch: 'full', component: ShoppingDetailsComponent},
      {path: 'list/:id/edit', pathMatch: 'full', component: ShoppingEditComponent},

      // Login
      {path: 'login', pathMatch: 'full', component: LoginComponent},
      {path: 'register', pathMatch: 'full', component: RegisterComponent},

    ],
  },

  {path: '**', pathMatch: 'full', redirectTo: 'dashboard'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

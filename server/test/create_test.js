const assert = require('assert')
const user = require('../models/user')
const product = require('../models/product')
const shopList = require('../models/shopList')
const group = require('../models/group')
const User = require("../models/user");

describe('Creating records', () => {
  it('saves a user', (done) => {
    const jeff = new user({
      name: "Jeff",
      email: "jeff@gmail.com",
      password: "hashedPassword"
    })
    jeff.save()
      .then(() => {
        assert(!jeff.isNew)
        done();
      })
  })
  it('saves a Product', (done) => {
    const carrot = new product({
      name: "Jeff",
      price: 4
    })
    carrot.save()
      .then(() => {
        assert(!carrot.isNew)
        done()
      })
  })
  it('saves a shopList', (done) => {
    const familyList = new shopList({
      name: "Family List"
    })
    familyList.save()
      .then(() => {
        assert(!familyList.isNew)
        done()
      })
  })
  it('saves a group', (done) => {
    const familyGroup = new group({
      name: "Family List",
      owner: new User() // Required
    })
    familyGroup.save()
      .then(() => {
        assert(!familyGroup.isNew)
        done()
      })
  })
})

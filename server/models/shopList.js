const mongoose = require('mongoose')
const Schema = mongoose.Schema
const productSchema = require('./productSchema')

const shopListSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required']
  },
  products: [productSchema],
  group: {
    type: Schema.Types.ObjectId,
    ref: 'group'
  }
})

module.exports = mongoose.model('shopList', shopListSchema)

describe('Register', () => {
  it('Visits my app', () => {
    cy.visit('/register')

    cy.url().should('include', '/register')
    cy.get('mat-label').contains('Name').should('have.text', 'Name')
    cy.get('mat-label').contains('Email').should('have.text', 'Email')
    cy.get('mat-label').contains('Password').should('have.text', 'Password')
  })
})

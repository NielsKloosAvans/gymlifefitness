const assert = require('assert')
const request = require('supertest')
const app = require('../../app')

const Product = require('../../models/product')

describe(('Product controller'), () => {
  let id;
  beforeEach((done) => {
    request(app)
      .post('/api/products')
      .send({
        name: "Wortel",
        price: 6
      })
      .end((err, res) => {
        id = res.body._id
        done()
      })
  })
  it('Post to /api/products creates a new product', done => {
    Product.countDocuments().then(count => {
      request(app)
        .post('/api/products')
        .send({
          name: "Wortel",
          price: 6
        })
        .end(() => {
          Product.countDocuments().then(newCount => {
            assert(count + 1 === newCount)
            done()
          })
        })
    })
  })
  it('Get to /api/products returns a list of products', done => {
    request(app)
      .get('/api/products')
      .end((err, res) => {
        assert(res.body[0] != null)
        done()
      })
  })
  it('Put to /api/products/:id updated a product', done => {
    request(app)
      .put(`/api/products/${id}`)
      .send({name: 'Carrot'})
      .end((err, res) => {
        assert(res.body.name === 'Carrot')
        done()
      })
  })
  it('Delete to /api/products/:id deletes a product', done => {
    request(app)
      .delete(`/api/products/${id}`)
      .end((err, res) => {
        assert(res.body.message === 'Product deleted.')
        done()
      })
  })
})

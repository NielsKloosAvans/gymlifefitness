describe('Product test', () => {
    it('Visits the product page', () => {
      cy.visit('/products');
      cy.contains('Products');
      cy.contains('About');
      cy.contains('Angular');
      cy.contains('Register');
      cy.contains('Login');
      cy.get('.navbar-nav').should('have.length', 2);
      cy.get('.navbar-nav').should('not.contain', 'Users');
    });
  });
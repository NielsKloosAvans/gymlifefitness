import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  username = 'Menu';

  constructor(private auth: AuthService, private router: Router) {
  }

  userExists(): boolean {
    return this.auth.userExists();
  }

  ngOnInit(): void {
    this.auth.getUserFromLocalStorage().subscribe(res => {
      if (res != null) {
        this.username = res.name.charAt(0).toUpperCase() + res.name.slice(1);
      }
    });
  }

  logout(): void {
    this.auth.logout();
    this.router.navigate(['/']).then(() => console.log('logged out'));
  }
}

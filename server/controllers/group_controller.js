const Group = require('../models/group')
const User = require('../models/user')

module.exports = {
  index(req, res, next) {
    User.findById(req.userData._id)
      .then(user => {
        res.send(user.groups)
      })
      .catch(next)
  },
  details(req, res, next) {
    Group.findById(req.params.id)
      .populate('owner')
      .populate('members')
      .then(group => {
        res.send(group)
      })
      .catch(next)
  },
  create(req, res, next) {
    Group.create({
      name: req.body.name,
      members: req.userData,
      owner: req.userData
    }).then((group) => {
      const groupCreated = group;
      User.findByIdAndUpdate(req.userData._id, {$push: {groups: group}})
        .then(() => res.send(groupCreated))
    })
      .catch(next)
  },
  put(req, res, next) {
    Group.findByIdAndUpdate(req.params.id, req.body)
      .then(() => Group.findById({_id: req.params.id}))
      .then(product => res.send(product))
      .catch(next)
  },
  delete(req, res, next) {
    Group.findByIdAndDelete(req.params.id)
      .then(group => {
        const groupCreated = group;
        User.findByIdAndUpdate(req.userData._id, {$pull: {groups: group}})
          .then(() => {
            res.status(200).send({
              message: 'Group deleted.',
              body: groupCreated
            })
          })
          .catch(next)
      })
      .catch(next)
  }
}

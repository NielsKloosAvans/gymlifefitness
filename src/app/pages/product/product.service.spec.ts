import { TestBed } from '@angular/core/testing';
import { ProductService } from './product.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {OverlayModule} from '@angular/cdk/overlay';
import {Product} from './product.model';

describe('ProductService', () => {
  let service: ProductService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatSnackBarModule, OverlayModule],
      providers: [ProductService]
    });
    service = TestBed.inject(ProductService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  xit('should retrieve items from API', () => {
    const dummyProducts: Product[] =
      [{
        _id: 'id',
        name: 'name',
        price: 0,
        category: 'category'
      }];

    service.getItems().subscribe(products => {
      expect(products).toEqual(dummyProducts);
    });
    const request = httpMock.expectOne('https://niels-cswf.herokuapp.com/api/products');
    expect(request.request.method).toBe('GET');
    request.flush(dummyProducts);
  });

});

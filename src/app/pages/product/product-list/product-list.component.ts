import {Component, OnInit} from '@angular/core';
import {Product} from '../product.model';
import {ProductService} from '../product.service';
import {ShoppingList} from '../../shopping/ShoppingList.model';
import {ShoppinglistService} from '../../shopping/shoppinglist.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  product$: Product[];
  lists: ShoppingList[];
  selList: string;

  constructor(
    private productService: ProductService,
    private shoppingListService: ShoppinglistService,
    private snackBar: MatSnackBar) {}

  getLists(): void {
    this.shoppingListService.getList().subscribe(res => {
      this.lists = res;
    });
  }

  getItems(): void {
    this.productService.getItems()
      .subscribe(products => this.product$ = products);
  }

  ngOnInit(): void {
    this.getItems();
    this.getLists();
  }

  deleteProduct(product: Product): void {
    this.productService.deleteItem(product)
      .subscribe(res => {
        console.log('Deleted product: ', res);
      });
  }

  onSelect(product: Product): void {
    if (this.selList != null) {
      console.log(`adding ${product.name} on list ${this.selList}`);
      this.productService.addToList(product, this.selList)
        .subscribe(res => {
          console.log('Added product to list: ', res);
        });
    } else {
      this.snackBar.open('Please select a shopping list', '', {
        duration: 2000
      });
    }
  }
}

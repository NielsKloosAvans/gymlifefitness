import {TestBed} from '@angular/core/testing';

import {CookiesHandlerService} from './cookies-handler.service';

describe('CookiesHandlerService', () => {
  let service: CookiesHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CookiesHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

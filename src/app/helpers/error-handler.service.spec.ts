import {TestBed} from '@angular/core/testing';

import {ErrorHandlerService} from './error-handler.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';

describe('ErrorHandlerService', () => {
  let service: ErrorHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule]
    });
    service = TestBed.inject(ErrorHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

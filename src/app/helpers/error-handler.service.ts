import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(private snackBar: MatSnackBar) {
  }

  public handleError<T>(operation = 'operation', response: any): any {
    console.error(`${operation} failed: ${response.error}`);
    this.snackBar.open(response.error.error, '', {
      duration: 2000
    });
    return of(undefined);
  }
}
